# Docker Init

A very simple init script for docker containers. The init script looks for certain
files on the root of the docker container file system:

`/init/before.d/*`      Scripts to run immediately before the container starts.
`/init/after.d/*`       Scripts to run as soon as a shutdown is in progress.
`/init.json`            Declares what processes to run.
`/init.yourname.json`   To extend the values in the main json


## `init.json`

```
{
    "processes": [
        {
            "title": "MariaDB"
            "cmd": "/usr/bin/mariadbd",
            "args": ["--skip-name-resolve", "--skip-networking", "--datadir=/srv/mariadb"],
        }
    ],
    "log-map": {
        "/var/logs/auth.log" => "auth.log"
    }
}

### `processes`

Declares each process that should run continously. Whenever the process dies, it is automatically
restarted.
